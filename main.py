import streamlit as st
from dotenv import load_dotenv
import os
import requests

# Load environment variables from .env file
load_dotenv()

# Retrieve API token from environment variables
API_TOKEN = os.getenv("API_TOKEN")

# Check if API token is set
if not API_TOKEN:
    st.error("API_TOKEN is not set. Please set it in your environment variables.")
    st.stop()

# Define headers with authorization token
HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}

# Define API URL for text generation model
API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"


def main():
    # Set title for Streamlit app
    st.title("Nutrition Needed for Symptoms")

    # Prompt user to enter text
    prompt = st.text_input("Enter prompt:", "")

    # Generate text when button is clicked
    if st.button("Generate Text"):
        with st.spinner("Generating..."):
            completions = get_completion(prompt)
            if completions:
                for completion in completions:
                    st.success(completion)
            else:
                # Display error message if text generation fails
                st.error("Failed to generate text. Please try again.")


def get_completion(prompt):
    # Construct prompt text
    prompt_text = f"I'm having {prompt}, what nutrition do I need?"

    try:
        # Send POST request to model API
        response = requests.post(API_URL, headers=HEADERS, json={"inputs": prompt_text})

        # Check if request was successful
        if response.ok:
            # Parse JSON response
            try:
                response_data = response.json()
                # Extracting the generated text from the response
                generated_texts = response_data[0]["generated_text"]
                # Splitting the generated text by newline to get individual suggestions
                suggestions = generated_texts.split("\n")
                # Return only the first two suggestions
                return suggestions[:2]
            except Exception as e:
                # Handle JSON parsing error
                st.error(f"Error parsing response JSON: {e}")
                return None
        else:
            # Display error message if request fails
            st.error(f"Failed to generate text. Status code: {response.status_code}")
            return None
    except Exception as e:
        # Handle request sending error
        st.error(f"Error sending request: {e}")
        return None


if __name__ == "__main__":
    main()
