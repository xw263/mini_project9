FROM python:3.11-slim

WORKDIR /app

COPY requirements.txt ./requirements.txt

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    netbase \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update -y && apt-get install -y gcc  python3-dev

RUN pip3 install -r requirements.txt

EXPOSE 8501
# copy everything in my static folder
COPY . .
COPY static /app/static

ENTRYPOINT ["streamlit", "run"]

CMD ["main.py"]