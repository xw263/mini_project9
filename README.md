[![pipeline status](https://gitlab.com/xw263/mini_project9/badges/main/pipeline.svg)](https://gitlab.com/xw263/mini_project9/-/commits/main)

# Mini_project9

## WebApp
http://3.81.71.204:8501/

![Screenshot_2024-04-06_at_8.35.41_PM](/uploads/2e1da57a84be065e28bc013c3026d19e/Screenshot_2024-04-06_at_8.35.41_PM.png)

![Screenshot_2024-04-06_at_7.20.23_PM](/uploads/a5f4eb6552a749039bc51a0a4bc7c044/Screenshot_2024-04-06_at_7.20.23_PM.png)

## Purpose
The goal of this project is to create a web app using LLM to generate search result and using streamlit as front end. Use  AWS to host it. The webapp I design is to generate the nutrition needed based on the symptoms given. 


## Steps
#### webApp Design
1. `pip install streamlit`
2. Choose an LLM model (eg: models from HuggingFace) to return the result of searching
3. Create an `.env file` and place your api token in the file
4. Design your streamlit app in `main.py`
5. Once you make the app do streamlit `run main.py` to test it locally it should go to http://localhost:8501
#### WebApp Deployment
1. Push your repo (needed for next step) to github
2. Sign into AWS to create your EC2 isntance
3. Launch Instance -> use `Ubuntu` as base image -> keep `t2.micro` -> allow `HTTP` and allow `HTTPS` -> edit -> add security group rule -> choose "CUSTOM TCP", port 8501, and choose source type "ANYWHERE"
4. Finish launching, wait for instance to provision fully
5. Click `connect to instance`, use the provided aws window
6. Set up you environment:
* `git clone your repo`
* `sudo apt update`
* `sudo apt-get install -y python3-pip`
* `cd to your repo`
* `pip3 install -r requirements.txt`
* `export PATH=~/.local/bin/:$PATH`
* `pip install --upgrade jinja2`
7. add your `.env file` -> `touch .env` -> `vi .env` -> copy your local `.env` and paste it in -> then type `:wq`
8. test your app by doing `streamlit run main.py`, this will give you the external link
9. After you confirm its exposed, set up a screen so you can close your session without terminating the app: type `screen`, press spacebar, then do `streamlit run main.py`
10. run your streamlit app and then detach the screen `ctrl` then `a` then `d`

## References
1. https://docs.streamlit.io/develop/tutorials/llms/build-conversational-apps#build-a-chatgpt-like-app
2. https://api-inference.huggingface.co/models/google/gemma-7b
3. https://gitlab.com/jeremymtan/jeremytan_ids721_week9