import os
from dotenv import load_dotenv
import requests

load_dotenv()
API_TOKEN = os.getenv("API_TOKEN")

HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"


def test_api():
    """checks hugging api token works"""
    test_payload = {"inputs": "The answer to the universe is [MASK]."}
    response = requests.post(API_URL, headers=HEADERS, json=test_payload)
    assert response.status_code == 200


if __name__ == "__main__":
    test_api()
